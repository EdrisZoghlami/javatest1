
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


 class CarTest {


	@Test
	public void testSpeed() {
		Car c= new Car(5);
		assertEquals(5, c.getSpeed());
	}

		
	
	@Test
	public void testLocation() {
		Car c= new Car(5);
		assertEquals(50, c.getLocation());
	}
	
	
	@Test
	public void testMoveRight() {
		Car c= new Car(5);
		c.moveRight();
		assertEquals(55, c.getLocation());
	}
	
	@Test
	public void testMoveLeft() {
		Car c= new Car(5);
		c.moveLeft();
		assertEquals(45, c.getLocation());
	}
	
	@Test
	public void testStop() {
		Car c= new Car(5);
		c.stop();
		assertEquals(0, c.getSpeed());
	}
	@Test
	public void testAccelerate() {
		Car c= new Car(5);
	
		c.accelerate();
		assertEquals(6, c.getSpeed());
	}

	

	}




