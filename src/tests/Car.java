package tests;

public class Car {
 private int speed;
 private int location;
 
 public static final int MAX_POSITION = 100;
 
 /**
  * Creates an initializes a Car object. The location of the Car is set to be
  * MAX_POSITION / 2
  * @param speed How fast the car initially goes. Must be >= 0
  */
 public Car(int speed) {
  if (speed < 0) {
   throw new IllegalArgumentException("Speed must not be negative!");
  }
  this.speed = speed;
  this.location = MAX_POSITION / 2;
 }
 
 /**
  * Gets the current speed of the Car.
  * @return Gets the value of the speed field.
  */
 public int getSpeed() {
  return this.speed;
 }
 
 /**
  * Gets the location of the Car.
  * @return Gets the value of the location field.
  */
 public int getLocation() {
  return this.location;
 }
 
 /**
  * Moves the Car to the right by adding the speed. If the Car is off the number line to the right, then set the position to be MAX_POSITION
  */
 public void moveRight() {
  this.location = this.location + this.speed;
  if (this.location > MAX_POSITION) {
   this.location = MAX_POSITION;
  }
 }
 
 /**
  * Moves the Car to the left by subtracting the speed. If the Car is off the number line to the left, then set the position to be 0.
  */
 public void moveLeft() {
  this.location = this.location - this.speed;
  if (this.location < 0) {
   this.location = 0;
  }
 }
 
 /**
  * Increases the speed by 1 unit.
  */
 public void accelerate() {
  this.speed++;
 }
 
 /**
  * Sets the Car's speed to be 0.
  */
 public void stop() {
  this.speed = 0;
 }
}

